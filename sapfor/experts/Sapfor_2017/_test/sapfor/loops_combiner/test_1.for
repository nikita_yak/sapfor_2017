      program loops_combiner_test
      implicit none
      parameter (l = 16,m = 6)
      real :: a(l),b(l),c(l)

      do it1 = 1,m
         do k1 = 1,l
            do p1 = 1, l + m
               a(k1) = it1 + k1 + p1
            enddo
         enddo
         do k1_2 = 1,l
            do p1_2 = 1, l + m
               b(k1_2) = it1 + k1_2 - p1_2
            enddo
         enddo
      enddo 

      do it2 = 1,m
         do  k2 = 1,l
            c(k2) = k2 - it2
         enddo  
      enddo 

      do it3 = 1,m
         do k3 = 1,l
            do p3 = 1, l + m
               a(k3) = it3 + k3 * p3
            enddo
         enddo
         do k3_2 = 1,l
            do p3_2 = 1, l + m
               b(k3_2) = it3 + k3_2 / p3_2
            enddo
         enddo
      enddo 

      end

